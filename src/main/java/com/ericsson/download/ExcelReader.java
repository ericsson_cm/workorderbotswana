package com.ericsson.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import com.ericsson.utils.AllUtilities;
import com.ericsson.wo.model.WorkOrder;

public class ExcelReader {
	private static String filePath;
	private static String DEFAULT_EXTENSION = "xls";
	private static String DEFAULT_SHEETNAME = "HDB-Data";

	ExcelReader(String filePath) {
		ExcelReader.filePath = filePath;
	}

	public static ArrayList<WorkOrder> readFile() {
		ArrayList result = new ArrayList();
		if (AllUtilities.ContainsExtension(filePath, DEFAULT_EXTENSION)) {
			try {
				FileInputStream e = new FileInputStream(new File(filePath));
				HSSFWorkbook workbook = new HSSFWorkbook(e);
				Sheet sheet = workbook.getSheet(DEFAULT_SHEETNAME);
				boolean flag = false;
				Iterator arg5 = sheet.iterator();

				while (arg5.hasNext()) {
					Row row = (Row) arg5.next();
					if (flag) {
						WorkOrder rowContent = new WorkOrder();
						int noOfColumns = row.getPhysicalNumberOfCells();
						if (row.getLastCellNum() > 177) {
							rowContent.setTaskkey(readNumericContentCell(row.getCell(0, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setNocRefId(readContentCell(row.getCell(1, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setCustomer(readContentCell(row.getCell(4, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setManuallyCreated(readContentCell(row.getCell(5, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTitle(readContentCell(row.getCell(11, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setPriority(readIntegerContentCell(row.getCell(12, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setStatus(readContentCell(row.getCell(13, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setSite(readContentCell(row.getCell(20, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setWoType(readContentCell(row.getCell(21, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setCreationDate(readDateContentCell(row.getCell(31, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setAssignmentStartDate(
									readDateContentCell(row.getCell(33, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setAssignmentFinishDate(
									readDateContentCell(row.getCell(34, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setAssignedFT(readContentCell(row.getCell(35, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setServiceArea3(readContentCell(row.getCell(42, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setServiceArea4(readContentCell(row.getCell(43, Row.CREATE_NULL_AS_BLANK)));
							rowContent
									.setLastStatusDate(readDateContentCell(row.getCell(46, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setReasonforStatusChange(
									readContentCell(row.getCell(47, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setSolutionReport(readContentCell(row.getCell(50, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInUnassigned(
									readTimeContentCell(row.getCell(57, Row.CREATE_NULL_AS_BLANK)));
							rowContent
									.setTimeInAssigned(readTimeContentCell(row.getCell(58, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInDispatched(
									readTimeContentCell(row.getCell(59, Row.CREATE_NULL_AS_BLANK)));
							
							//rowContent.setTimeInAccepted(readTimeContentCell(row.getCell(60, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInRejectedFromField(
									readTimeContentCell(row.getCell(61, Row.CREATE_NULL_AS_BLANK)));
							rowContent
									.setTimeInRejected(readTimeContentCell(row.getCell(64, Row.CREATE_NULL_AS_BLANK)));
							rowContent
									.setTimeInCancelled(readTimeContentCell(row.getCell(65, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInWaitingInternal(
									readTimeContentCell(row.getCell(66, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInWaitingExternal(
									readTimeContentCell(row.getCell(67, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInInProgress(
									readTimeContentCell(row.getCell(68, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInRequestReOpen(
									readTimeContentCell(row.getCell(69, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInObjectReady(
									readTimeContentCell(row.getCell(75, Row.CREATE_NULL_AS_BLANK)));
							rowContent.setTimeInTravel(readTimeContentCell(row.getCell(76, Row.CREATE_NULL_AS_BLANK)));
							rowContent
									.setStatusBeforeChange(readContentCell(row.getCell(177, Row.CREATE_NULL_AS_BLANK)));
						}

						result.add(rowContent);
					} else {
						flag = true;
					}
				}
			} catch (FileNotFoundException arg8) {
				arg8.printStackTrace();
			} catch (IOException arg9) {
				arg9.printStackTrace();
			} catch(NullPointerException nullPointer){
				nullPointer.printStackTrace();
			}
		}

		return result;
	}

	private static String readContentCell(Cell currentCell) {
		String result = null;
		if (currentCell.getCellTypeEnum() == CellType.STRING) {
			result = currentCell.getStringCellValue();
		}

		return result;
	}

	private static int readIntegerContentCell(Cell currentCell) {
		int result = 0;
		if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
			result = (int) currentCell.getNumericCellValue();
		}

		return result;
	}

	private static DateTime readDateContentCell(Cell currentCell) {
		DateTime result = null;
		if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
			result = new DateTime(currentCell.getDateCellValue());
		}

		return result;
	}

	private static Period readTimeContentCell(Cell currentCell) {
		Period result = null;
		if ((currentCell.getCellTypeEnum() == CellType.NUMERIC) && (currentCell.getDateCellValue() != null)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = sdf.format(currentCell.getDateCellValue());
			String[] dateParts = dateStr.split(" ");
			String timeStr = "";
			if (dateParts.length > 0) {
				timeStr = dateParts[dateParts.length - 1];
				PeriodFormatter formatter = (new PeriodFormatterBuilder()).appendHours().printZeroIfSupported()
						.minimumPrintedDigits(2).appendSeparator(":").appendMinutes().printZeroIfSupported()
						.minimumPrintedDigits(2).appendSeparator(":").appendSeconds().printZeroIfSupported()
						.minimumPrintedDigits(2).appendSeparator(":").toFormatter();
				result = formatter.parsePeriod(timeStr);
			}
		}
		
		return result;
	}

	private static double readNumericContentCell(Cell currentCell) {
		double result = 0.0D;
		if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
			result = currentCell.getNumericCellValue();
		}

		return result;
	}

	private static DateTime parseDate(String text) {
		DateTime date = new DateTime(DateTimeZone.UTC);
		DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
		if (text != null) {
			date = dtf.withZone(DateTimeZone.UTC).parseDateTime(text);
		}

		return date;
	}

	private static DateTime parseTime(String text) {
		DateTime date = new DateTime(DateTimeZone.UTC);
		DateTimeFormatter dtf = DateTimeFormat.forPattern("HH:mm:ss");
		if (text != null) {
			date = dtf.withZone(DateTimeZone.UTC).parseDateTime(text);
		}

		return date;
	}

	public static File get() {
		return null;
	}
}
