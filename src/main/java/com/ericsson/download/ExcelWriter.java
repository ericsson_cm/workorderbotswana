package com.ericsson.download;

import com.ericsson.download.SendEmail;
import com.ericsson.utils.AllUtilities;
import com.ericsson.wo.model.WorkOrder;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ExcelWriter {
	DateTime now;
	LocalDate today;
	DateTimeFormatter fmt;
	Workbook workbook;
	Sheet sheet;
	ArrayList<WorkOrder> workOrders;
	private String excelFilePath;
	private static String DEFAULT_EXTENSION = "xls";
	private static String[] columns = new String[]{"Task Key", "Noc Ref Id", "Priority", "Time/SLA", "Status",
			"Status Before Change", "Time Since Last Status", "Current Time", "Region", "Assigned FT", "Office",
			"Title", "Creation Date", "Last Status Date", "Reason for Status Change", "WO Type", "Manually Created"};

	public ExcelWriter(ArrayList<WorkOrder> workOrders, String excelFilePath) {
		this.now = new DateTime(DateTimeZone.UTC);
		this.today = this.now.toLocalDate();
		this.fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm ");
		this.workbook = new HSSFWorkbook();
		this.sheet = this.workbook.createSheet("GLOBAL");
		this.excelFilePath = excelFilePath;
		this.workOrders = workOrders;
	}

	public void writeExcel(DateTime currentDate) throws IOException {
		this.writeGlobalSheet();
		this.writeFile();
	}

	public void writeGlobalSheet() {
		this.createHeaderRow(this.sheet, 3);
		this.writeSheet(this.sheet, this.workOrders, 3);
		this.createTitleHeaderRow(this.sheet, 2);
	}

	private void writeFile() throws IOException {
		FileOutputStream outputStream = null;

		try {
			outputStream = new FileOutputStream(this.excelFilePath);
			this.workbook.write(outputStream);
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}

		}

	}

	public void writeSheet(Sheet sheet, Collection<WorkOrder> workOrders, int rowCount) {
		Period zero = Period.ZERO;
		Iterator arg5 = workOrders.iterator();

		while (arg5.hasNext()) {
			WorkOrder workOrder = (WorkOrder) arg5.next();
			Period totalTime = workOrder.sumPeriod();
			if (AllUtilities.isPEriodEqualToZero(totalTime) && !workOrder.getStatus().equalsIgnoreCase("closed")
					&& !workOrder.getStatus().equalsIgnoreCase("Cancelled")
					&& !workOrder.getStatus().equalsIgnoreCase("Resolved")) {
				++rowCount;
				Row i = sheet.createRow(rowCount);

				try {
					this.writeWorkOrder(sheet, workOrder, i, rowCount > workOrders.size());
				} catch (FileNotFoundException arg9) {
					arg9.printStackTrace();
				} catch (IOException arg10) {
					arg10.printStackTrace();
				}
			}

			for (int arg11 = 1; arg11 < columns.length; ++arg11) {
				sheet.autoSizeColumn(arg11);
			}

			sheet.setColumnWidth(columns.length, 12000);
		}

	}

	private CellStyle getCellStyle(Sheet sheet, WorkOrder workOrder) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setColor((short) 8);
		cellStyle.setAlignment(HorizontalAlignment.LEFT);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setFillPattern((short) 1);
		cellStyle.setFillForegroundColor((short) 9);
		font.setBold(false);
		font.setFontHeightInPoints((short) 12);
		cellStyle.setFont(font);
		return cellStyle;
	}

	private void writeWorkOrder(Sheet sheet, WorkOrder workOrder, Row row, boolean end)
			throws FileNotFoundException, IOException {
		CellStyle cellStyle = this.getCellStyle(sheet, workOrder);
		CellStyle cellDateStyle = this.getCellStyle(sheet, workOrder);
		CellStyle cellLongStyle = this.getCellStyle(sheet, workOrder);
		CellStyle cellPeriodStyle = this.getCellStyle(sheet, workOrder);
		CellStyle cellDoubleStyle = this.getCellStyle(sheet, workOrder);
		CreationHelper createHelper = this.workbook.getCreationHelper();
		Cell cell = row.createCell(1);
		cell.setCellValue(workOrder.getTaskkey());
		cellStyle.setBorderLeft((short) 2);
		cellStyle.setFillForegroundColor((short) 9);
		cellLongStyle.setBorderLeft((short) 2);
		cellLongStyle.setFillForegroundColor((short) 9);
		cellPeriodStyle.setBorderLeft((short) 2);
		cellPeriodStyle.setFillForegroundColor((short) 9);
		cellDoubleStyle.setBorderLeft((short) 2);
		cellDoubleStyle.setFillForegroundColor((short) 9);
		cellDateStyle.setBorderLeft((short) 2);
		cellDateStyle.setFillForegroundColor((short) 9);
		cellStyle.setBorderBottom((short) 1);
		cellLongStyle.setBorderBottom((short) 1);
		cellPeriodStyle.setBorderBottom((short) 1);
		cellDoubleStyle.setBorderBottom((short) 1);
		cellDateStyle.setBorderBottom((short) 1);
		cellDateStyle.setBorderRight((short) 2);
		cellStyle.setBorderRight((short) 2);
		if (end) {
			cellStyle.setBorderBottom((short) 2);
			cellLongStyle.setBorderBottom((short) 2);
		}

		cellLongStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
		cell.setCellStyle(cellLongStyle);
		cell = row.createCell(2);
		cell.setCellValue(workOrder.getNocRefId());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(3);
		cell.setCellValue((double) workOrder.getPriority());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(13);
		cell.setCellValue(AllUtilities.convertJodaTime(workOrder.getCreationDate()));
		cellDateStyle.setBorderRight((short) 2);
		cellDateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));
		cellDateStyle.setBorderBottom((short) 1);
		cellDateStyle.setFillPattern((short) 1);
		cellDateStyle.setBorderLeft((short) 2);
		cellDateStyle.setBorderTop((short) 2);
		cell.setCellStyle(cellDateStyle);
		Period Currenttime = AllUtilities.getAbsDifference(workOrder.getCreationDate(), this.now);
		cell = row.createCell(8);
		cell.setCellValue(AllUtilities.parsePeriod(Currenttime));
		cell.setCellStyle(cellStyle);
		double timeSLA = AllUtilities.dividePeriod(Currenttime, AllUtilities.getPriorityTime(workOrder.getPriority()));
		cellDoubleStyle.setBorderRight((short) 2);
		cell = row.createCell(4);
		cellDoubleStyle.setDataFormat(createHelper.createDataFormat().getFormat("#.## %"));
		cell.setCellValue(timeSLA);
		if (timeSLA == 0.0D) {
			cellDoubleStyle.setDataFormat(createHelper.createDataFormat().getFormat("0.00 %"));
			cellDoubleStyle.setFillForegroundColor((short) 9);
		} else if (timeSLA < 0.1D) {
			cellDoubleStyle.setDataFormat(createHelper.createDataFormat().getFormat("0.## %"));
		} else if (timeSLA <= 0.25D) {
			cellDoubleStyle.setFillForegroundColor((short) 9);
		} else if (timeSLA <= 0.5D) {
			cellDoubleStyle.setFillForegroundColor((short) 17);
		} else if (timeSLA <= 0.75D) {
			cellDoubleStyle.setFillForegroundColor((short) 13);
		} else if (timeSLA <= 1.0D) {
			cellDoubleStyle.setFillForegroundColor((short) 53);
		} else if (timeSLA > 1.0D) {
			cellDoubleStyle.setFillForegroundColor((short) 10);
		} else {
			createHelper.createDataFormat().getFormat("0.00 %");
		}

		cell.setCellStyle(cellDoubleStyle);
		cellDoubleStyle.setFillPattern((short) 1);
		cellDoubleStyle.setBorderLeft((short) 2);
		cellDoubleStyle.setBorderBottom((short) 2);
		cellDoubleStyle.setBorderTop((short) 2);
		cellDoubleStyle.setBorderRight((short) 2);
		cell.setCellStyle(cellDoubleStyle);
		cell = row.createCell(14);
		cell.setCellValue(AllUtilities.convertJodaTime(workOrder.getLastStatusDate()));
		cellDateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy hh:mm:ss"));
		cellDateStyle.setBorderBottom((short) 1);
		cellDateStyle.setFillPattern((short) 1);
		cellDateStyle.setBorderLeft((short) 2);
		cellDateStyle.setBorderTop((short) 2);
		cellDateStyle.setBorderRight((short) 2);
		cell.setCellStyle(cellDateStyle);
		Period TimeSinceLastStatus = AllUtilities.getAbsDifference(workOrder.getLastStatusDate(), this.now);
		cell = row.createCell(7);
		
		try{
			cell.setCellValue(AllUtilities
					.parsePeriod(AllUtilities.getAbsDifference(workOrder.getCreationDate(), workOrder.getLastStatusDate())));
		}catch(IllegalArgumentException e){
		}finally{
			cell.setCellValue(AllUtilities
					.parsePeriod(AllUtilities.getAbsDifference(workOrder.getCreationDate(), workOrder.getCreationDate().plusHours(1))));
		}
		
		cell.setCellValue(AllUtilities.parsePeriod(TimeSinceLastStatus));
		cell.setCellStyle(cellStyle);
		cell = row.createCell(6);
		cell.setCellValue(workOrder.getStatusBeforeChange());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(5);
		cell.setCellValue(workOrder.getStatus());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(9);
		cell.setCellValue(workOrder.getServiceArea3());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(10);
		cell.setCellValue(workOrder.getAssignedFT());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(11);
		cell.setCellValue(workOrder.getServiceArea4());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(12);
		cell.setCellValue(workOrder.getTitle());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(15);
		cell.setCellValue(workOrder.getReasonforStatusChange());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(16);
		cell.setCellValue(workOrder.getWoType());
		cell.setCellStyle(cellStyle);
		Period PriorityTime = AllUtilities.getPriorityTime(workOrder.getPriority());
		cell.setCellStyle(cellStyle);
		cell = row.createCell(17);
		cell.setCellValue(workOrder.getManuallyCreated());
		cellStyle.setFillForegroundColor((short) 9);
		cellStyle.setBorderRight((short) 2);
		cellStyle.setFillPattern((short) 1);
		cellStyle.setBorderLeft((short) 2);
		cellStyle.setBorderBottom((short) 2);
		cellStyle.setBorderTop((short) 2);
		cell.setCellStyle(cellStyle);
		cell.setCellStyle(cellStyle);
		new SendEmail();
	}

	private void createHeaderRow(Sheet sheet, int rowCount) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		cellStyle.setFillForegroundColor((short) 24);
		cellStyle.setFillPattern((short) 1);
		cellStyle.setBorderLeft((short) 2);
		cellStyle.setBorderBottom((short) 2);
		cellStyle.setBorderTop((short) 2);
		cellStyle.setBorderRight((short) 2);
		font.setColor((short) 18);
		cellStyle.setFont(font);
		cellStyle.setAlignment(HorizontalAlignment.LEFT);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		Row row = sheet.createRow(rowCount);

		for (int i = 0; i < columns.length; ++i) {
			Cell cell = row.createCell(i + 1);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(columns[i]);
		}

	}

	public ArrayList<WorkOrder> filterWO() throws FileNotFoundException, IOException {
		Period zero = Period.ZERO;
		ArrayList result = new ArrayList();
		Iterator arg3 = this.workOrders.iterator();

		while (arg3.hasNext()) {
			WorkOrder workOrder = (WorkOrder) arg3.next();
			Period totalTime = workOrder.sumPeriod();
			if (AllUtilities.isPEriodEqualToZero(totalTime) && !workOrder.getStatus().equalsIgnoreCase("closed")
					&& !workOrder.getStatus().equalsIgnoreCase("Resolved")) {
				result.add(workOrder);
			}
		}

		this.workOrders = result;
		return result;
	}

	private void createTitleHeaderRow(Sheet sheet, int rowCount) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		Font font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 16);
		font.setColor((short) 10);
		cellStyle.setFont(font);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		Row emptyRow = sheet.createRow(rowCount);
		cellStyle.setFillForegroundColor((short) 9);
		cellStyle.setFillPattern((short) 1);
		cellStyle.setBorderLeft((short) 2);
		cellStyle.setBorderBottom((short) 2);
		cellStyle.setBorderTop((short) 2);
		cellStyle.setBorderRight((short) 2);
		CellRangeAddress emptyCellRangeAddress = new CellRangeAddress(rowCount, rowCount, 1, columns.length);
		RegionUtil.setBorderTop(2, emptyCellRangeAddress, sheet, this.workbook);
		RegionUtil.setBorderLeft(2, emptyCellRangeAddress, sheet, this.workbook);
		RegionUtil.setBorderRight(2, emptyCellRangeAddress, sheet, this.workbook);
		RegionUtil.setBorderBottom(1, emptyCellRangeAddress, sheet, this.workbook);
		sheet.addMergedRegion(emptyCellRangeAddress);
		Cell cellRegion = emptyRow.createCell(1);
		cellRegion.setCellValue("REPORT OF " + this.fmt.withZone(DateTimeZone.forID("Africa/Douala")).print(this.now));
		cellRegion.setCellStyle(cellStyle);
	}
}