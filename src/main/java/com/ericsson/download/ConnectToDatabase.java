package com.ericsson.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectToDatabase {
	public static void main(String[] argv) {
		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException arg3) {
			System.out.println("Where is your MySQL JDBC Driver?");
			arg3.printStackTrace();
			return;
		}

		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;

		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ericsson?useSSL=false", "root",
					"");
		} catch (SQLException arg2) {
			System.out.println("Connection Failed! Check output console");
			arg2.printStackTrace();
			return;
		}

		if (connection != null) {
			System.out.println("I made it, i can take control to my database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

	}
}
