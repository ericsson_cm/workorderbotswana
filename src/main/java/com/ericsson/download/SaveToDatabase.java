package com.ericsson.download;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ericsson.utils.AllUtilities;
import com.ericsson.wo.model.WorkOrder;

public class SaveToDatabase {
	private ArrayList<WorkOrder> workOrders = new ArrayList();
	private List<WorkOrder> result = new ArrayList();
	private static String excelFilePath = "Hdb_Result.xls";
	private static String[] columns = new String[]{"ID", "DATE_OF_REPORT", "TASK_Key", "NOC_Ref_Id", "Priority",
			"TimeSLA", "Status", "Status_Before_Change", "Time_Since_Last_Status", "Actual_Time", "Region",
			"Assigned_FT", "Office", "Title", "Creation_Date", "Last_Status_Date", "Reason_for_Status_Change",
			"WO_Type", "Manually_Created"};
	HSSFWorkbook workbook = new HSSFWorkbook();
	Sheet sheet;

	private Connection startConnection() {
		Connection result = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException arg3) {
			arg3.printStackTrace();
			return result;
		}

		try {
			result = DriverManager.getConnection("jdbc:mysql://localhost:3306/ericsson?useSSL=false", "root",
					"");
			return result;
		} catch (SQLException arg2) {
			arg2.printStackTrace();
			return result;
		}
	}

	public SaveToDatabase(ArrayList<WorkOrder> workOrders, String excelFilePath) {
		this.sheet = this.workbook.getSheet("GLOBAL");
		this.workOrders = workOrders;
		SaveToDatabase.excelFilePath = excelFilePath;
	}

	public void save() throws IOException {
		Connection connection = this.startConnection();

		for (int i = 0; i < this.workOrders.size(); ++i) {
			saveIntoDatabase(connection, excelFilePath, (WorkOrder) this.workOrders.get(i));
		}

	}

	public ArrayList<WorkOrder> filterWO(ArrayList<WorkOrder> workOrders) throws FileNotFoundException, IOException {
		Period zero = Period.ZERO;
		ArrayList result = new ArrayList();
		Iterator arg4 = workOrders.iterator();

		while (arg4.hasNext()) {
			WorkOrder workOrder = (WorkOrder) arg4.next();
			Period totalTime = workOrder.sumPeriod();
			if (AllUtilities.isPEriodEqualToZero(totalTime) && !workOrder.getStatus().equalsIgnoreCase("closed")
					&& !workOrder.getStatus().equalsIgnoreCase("Resolved")) {
				result.add(workOrder);
			}
		}

		return result;
	}

	public static void saveIntoDatabase(Connection connection, String excelFilePath, WorkOrder workOrder) {
		DateTime now = new DateTime(DateTimeZone.UTC);
		LocalDate today = now.toLocalDate();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String NocRefId = workOrder.getNocRefId();
		String customer = workOrder.getCustomer();
		String manuallyCreated = workOrder.getManuallyCreated();
		String title = workOrder.getTitle();
		int priority = workOrder.getPriority();
		Period Currenttime = AllUtilities.getAbsDifference(workOrder.getCreationDate(), now);
		Period TimeSinceLastStatus = AllUtilities.getAbsDifference(workOrder.getLastStatusDate(), now);
		double timeSLA = AllUtilities.dividePeriod(Currenttime, AllUtilities.getPriorityTime(workOrder.getPriority()));
		double taskkey = workOrder.getTaskkey();
		String status = workOrder.getStatus();
		String FSO = workOrder.getFSO();
		String cluster = workOrder.getCluster();
		String woType = workOrder.getWoType();
		DateTime creationDate = workOrder.getCreationDate();
		String assignedFT = workOrder.getAssignedFT();
		String serviceArea3 = workOrder.getServiceArea3();
		String serviceArea4 = workOrder.getServiceArea4();
		DateTime lastStatusDate = workOrder.getLastStatusDate();
		String reasonforStatusChange = workOrder.getReasonforStatusChange();
		String statusBeforeChange = workOrder.getStatusBeforeChange();
		String dateofreport = fmt.withZone(DateTimeZone.forID("Africa/Gaborone")).print(now);
		String id = taskkey + dateofreport;
		/*if (assignedFT != null && assignedFT.equalsIgnoreCase("3PP IHS NOC CM")) {
			FSO = assignedFT;
		} else {
			FSO = serviceArea3;
		}*/
		if (assignedFT != null && check_camusat(assignedFT)) {
			//FSO = assignedFT;
			FSO = "BW Camusat";
		} else {
			FSO = serviceArea3;
		}

		if (assignedFT != null && assignedFT.equalsIgnoreCase("Aubin Nguegang Fomo")) {
			cluster = "MESSAMENDONG";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Alain Raoul Tchatchet Mbouya")) {
			cluster = "BERTOUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Armand Nguetsa Sonkeng")) {
			cluster = "CBC";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Bakirou")) {
			cluster = "NKOGEDZEN";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Camille Nguekam")) {
			cluster = "BONABERI";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Christian Angong")) {
			cluster = "NGOAEKELE";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Clauvis Cherif Elangman")) {
			cluster = "BERTOUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Edgard Desire Oyen")) {
			cluster = "DJEMOUN";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Jean Emmanuel Atangana Atangana")) {
			cluster = "BUEA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Etienne Fomin")) {
			cluster = "FOUMBAN";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Flore Ntyam")) {
			cluster = "EBOLOWA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Guy Bertrand Kana")) {
			cluster = "BAMENDA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Landry Serge Pegha Yemih")) {
			cluster = "NGAOUNDERE";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("LISSOM RICHARD")) {
			cluster = "YAGOUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Mouamadou Mouamadou")) {
			cluster = "NGAOUNDAL";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Nathanus Blakang")) {
			cluster = "NDOGBONG";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Paul Ondoua Dit Zang")) {
			cluster = "KRIBI";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Richard Fibassou")) {
			cluster = "GAROUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Romeo Alain Aime Samou")) {
			cluster = "MAROUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Serge Olivier Gnokon")) {
			cluster = "GAROUA";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Joseph Simo")) {
			cluster = "NDOGBONG";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Thomas Dim")) {
			cluster = "DJEMOUN";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Willy Ngougni")) {
			cluster = "KOUSSERI";
		} else if (assignedFT != null && assignedFT.equalsIgnoreCase("Gilles Henri Yene Toner")) {
			cluster = "FRANQUEVILLE";
		}

		
		try {
			PreparedStatement e = connection.prepareStatement(
					"INSERT INTO work_order_t(ID,DATE_OF_REPORT,TASK_Key,NOC_Ref_Id,Priority,TimeSLA,Status,Status_Before_Change,Time_Since_Last_Status,Actual_Time,Region,FSO,Assigned_FT,Office,Title,Creation_Date,Last_Status_Date,Reason_for_Status_Change,WO_Type,Manually_Created) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			e.setString(1, id);
			e.setString(2, dateofreport);
			e.setDouble(3, taskkey);
			e.setString(4, NocRefId);
			e.setInt(5, priority);
			e.setDouble(6, timeSLA);
			e.setString(7, status);
			e.setString(8, statusBeforeChange);
			e.setString(9, AllUtilities.parsePeriod(TimeSinceLastStatus));
			e.setString(10, AllUtilities.parsePeriod(Currenttime));
			e.setString(11, serviceArea3);
			e.setString(12, FSO);
			e.setString(13, assignedFT);
			e.setString(14, serviceArea4);
			e.setString(15, title);
			e.setString(16, AllUtilities.parseDate(creationDate));
			e.setString(17, AllUtilities.parseDate(lastStatusDate));
			e.setString(18, reasonforStatusChange);
			e.setString(19, woType);
			e.setString(20, manuallyCreated);
			boolean res = e.execute();
			e.close();
		} catch (SQLException arg31) {
			arg31.printStackTrace();
		} catch (NullPointerException arg32) {
			arg32.printStackTrace();
		}

	}
	
	private static boolean check_camusat(String init){
		/*Pattern p = Pattern.compile("[^a-zA-Z ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(init);
		boolean b = m.find();

		if (b)
		   return true;*/
		String check = "Camusat";
		if(init.toLowerCase().contains(check.toLowerCase()))
			return true;
		return false;
	}
}
