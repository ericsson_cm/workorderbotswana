package com.ericsson.download;

import com.ericsson.download.ExcelReader;
import com.ericsson.download.ExcelWriter;
import com.ericsson.download.Hdbdownload;
import com.ericsson.download.SaveToDatabase;
import com.ericsson.wo.model.WorkOrder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class MainDownload {

	public static void main(String[] args) throws Throwable {
		String url = "jdbc:mysql://localhost:3306/ericsson?useSSL=false";
		Connection conn = DriverManager.getConnection(url, "root", "");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select username,password from connexion_wfm_botswana where profile=\'thatayaone\'");

		while (rs.next()) {
			String username = rs.getString("username");
			String password = rs.getString("password");
			String filename = "Hdb_Export.xls";
			String filePath = "Hdb_Export.xls";
			String excelFilePath = "Hdb_Result.xls";
			FirstLoginPage firstPage = new FirstLoginPage();
			Hdbdownload Hdbdownload = new Hdbdownload();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.getSheet("GLOBAL");

			try {
				firstPage.post_data(username, password);
				Hdbdownload.downloadExcelFile(username, password, filename);
			} catch (Exception arg18) {
				arg18.printStackTrace();
			}

			new ExcelReader(filePath);
			new ArrayList<Object>();
			
			try{
				ArrayList<WorkOrder> wos = ExcelReader.readFile();
				System.out.println("MainDownload:Reading Done !" + wos.size());
				ExcelWriter excelWriter = new ExcelWriter(wos, excelFilePath);
				new ArrayList<Object>();
				ArrayList<WorkOrder> workOrders = excelWriter.filterWO();
				DateTime currentDate = new DateTime(DateTimeZone.UTC);
				System.out.println(currentDate.getDayOfMonth() + " " + currentDate.getMonthOfYear() + currentDate.getYear());
				excelWriter.writeExcel(currentDate);
				System.out.println("MainDownload-90: Writing Done !" + workOrders.size());
				SaveToDatabase savetoDatabase = new SaveToDatabase(workOrders, excelFilePath);
				savetoDatabase.save();
				System.out.println(" Save ok to Database" + workOrders.size());
				System.exit(0);
			}catch(NullPointerException e){
				//e.printStackTrace();
				System.out.println(e.getMessage());
			}finally{
				//ArrayList<WorkOrder> wos = ExcelReader.readFile();
				
			}
		}

	}

}
