package com.ericsson.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	private static ConnectionFactory instance = new ConnectionFactory();
	public static final String URL = "jdbc:mysql://localhost:3306/ericsson";
	public static final String USER = "root";
	public static final String PASSWORD = " ";
	public static final String DRIVER = "com.mysql.jdbc.Driver";

	private ConnectionFactory() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException arg1) {
			arg1.printStackTrace();
		}

	}

	private Connection createConnection() {
		Connection connection = null;

		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ericsson", "root", " ");
		} catch (SQLException arg2) {
			System.out.println("ERROR: Unable to Connect to Database.");
		}

		return connection;
	}

	public static Connection getConnection() {
		return instance.createConnection();
	}
}
