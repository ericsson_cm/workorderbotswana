package com.ericsson.download;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.sun.xml.internal.fastinfoset.Encoder;

public class FirstLoginPage {
	
	public void post_data(String username, String password) throws ClientProtocolException, IOException{
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://access.se.msdp.ericsson.net/nf/auth/doAuthentication.do");

		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("login", username));
		params.add(new BasicNameValuePair("passwd", password));
		httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity entity = response.getEntity();

		if (entity != null) {
		    InputStream instream = entity.getContent();
		    try {
		    	String theString = IOUtils.toString(instream, Encoder.UTF_8); 
		        System.out.println(theString);
		        Header[] g = response.getAllHeaders();
		        for(Header h: g){
		        	System.out.println(h.getName() + " = " + h.getValue());
		        }
		        System.out.println(response.getAllHeaders().length);
		    } finally {
		        instream.close();
		       
		    }
		}
	}
}
