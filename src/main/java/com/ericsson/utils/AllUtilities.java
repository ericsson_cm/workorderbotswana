package com.ericsson.utils;

import com.ericsson.wo.model.WorkOrder;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class AllUtilities {
	static String[] periodList = new String[]{"03:45:00", "07:45:00", "15:45:00", "147:45:00", "147:45:00"};
	static Period[] periods = null;

	static {
		buildPeriod();
	}

	public static Date convertJodaTime(DateTime dateTime) {
		Date dateNew = DateTime.now().toDate();
		if (dateTime != null) {
			dateNew = dateTime.toDate();
		}

		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss");
		return dateNew;
	}

	public static java.sql.Date convertJodaTimeToSQL(DateTime myDateTime) {
		Calendar cal = Calendar.getInstance();
		cal.get(11);
		cal.get(12);
		cal.get(13);
		java.sql.Date dateForMySql = new java.sql.Date(cal.getTime().getTime());
		return dateForMySql;
	}

	public static long convertToSqlDateTime(DateTime dateTime) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		return Long.valueOf(dateTime.toString(formatter)).longValue();
	}

	public static long convertToSqlPeriod(Period period) {
		return (long) (period.getHours() * 3600 + period.getMinutes() * 60 + period.getSeconds());
	}

	public static Period convertJodaPeriod(Period period) {
		Period periodNew = period;
		if (period != null) {
			periodNew = period.toPeriod();
		}

		return periodNew;
	}

	public static void buildPeriod() {
		int size = periodList.length;
		periods = new Period[size];

		for (int i = 0; i < size; ++i) {
			periods[i] = parseTime(periodList[i]);
		}
	}

	public static Period getPriorityTime(int priority) {
		Period period = null;
		if (priority > 0) {
			period = periods[priority - 1];
		}

		return period;
	}

	public static String formatDouble(double val) {
		return (new DecimalFormat("#.##")).format(val);
	}

	public static Period getDifference(DateTime startDate, DateTime endDate) {
		Interval interval = new Interval(startDate, endDate);
		Period period = interval.toPeriod();
		return period;
	}

	public static Period getAbsDifference(DateTime startDate, DateTime endDate) {
		Period period = null;
		if (startDate.isAfter(endDate)) {
			period = getDifference(endDate, startDate);
		} else {
			period = getDifference(startDate, endDate);
		}

		return period;
	}

	public static DateTime subPeriod(DateTime startDate, Period period) {
		DateTime result = startDate.minus(period);
		return result;
	}

	public static Period subPeriod(Period startPeriod, Period endPeriod) {
		Period result = endPeriod.minus(startPeriod);
		return result;
	}

	public static boolean isPEriodEqualToZero(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		int s = period.getSeconds();
		if (d == 0 && h == 0 && m == 0 && s == 0) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriodminute15(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h == 0 && m >= 15) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriodminute20(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h == 0 && m >= 20) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriodminute30(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h == 0 && m >= 30) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriod2hour(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h >= 2 && m == 0) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriod1hour(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h >= 1 && m == 0) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriod4hour(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h >= 4 && m == 0) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriod6hour(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d == 0 && h >= 6 && m == 0) {
			result = true;
		}

		return result;
	}

	public static boolean getPeriod24hour(Period period) {
		boolean result = false;
		int d = period.getDays();
		int h = period.getHours();
		int m = period.getMinutes();
		if (d >= 1 && h == 0 && m == 0) {
			result = true;
		}

		return result;
	}

	public static Period sumPeriod(Period startPeriod, Period endPeriod) {
		Period result = Period.ZERO;
		result = result.plus(endPeriod);
		result = result.plus(startPeriod);
		return result;
	}

	public static DateTime addPeriod(DateTime startDate, Period period) {
		DateTime result = startDate.plus(period);
		return result;
	}

	public static double dividePeriod(Period startPeriod, Period endPeriod) {
		double result = 1.0D;
		if (endPeriod != null) {
			long start = (long) (startPeriod.getDays() * 3600 * 24 + startPeriod.getHours() * 3600
					+ startPeriod.getMinutes() * 60 + startPeriod.getSeconds());
			long end = (long) (endPeriod.getDays() * 3600 * 24 + endPeriod.getHours() * 3600
					+ endPeriod.getMinutes() * 60 + endPeriod.getSeconds());
			result = (double) start / (double) end;
		}

		return result;
	}

	public static Period parseTime(String timeStr) {
		Period result = null;
		PeriodFormatter formatter = (new PeriodFormatterBuilder()).appendHours().printZeroIfSupported()
				.minimumPrintedDigits(2).appendSeparator(":").appendMinutes().printZeroIfSupported()
				.minimumPrintedDigits(2).appendSeparator(":").appendSeconds().printZeroIfSupported()
				.minimumPrintedDigits(2).appendSeparator(":").toFormatter();
		result = formatter.parsePeriod(timeStr);
		return result;
	}

	public static String getFileExtension(String filename) {
		return FilenameUtils.getExtension(filename);
	}

	public static boolean ContainsExtension(String filename, String extension) {
		String ext = getFileExtension(filename).toLowerCase();
		boolean result = false;
		if (ext.equals(extension.toLowerCase())) {
			result = true;
		}

		return result;
	}

	public static String parseDate(DateTime date) {
		String dateStr = "";
		if (date != null) {
			DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
			dateStr = fmt.withZone(DateTimeZone.forID("Africa/Gaborone")).print(date);
		}

		return dateStr;
	}

	public static Timestamp convertStringToTimestamp(String str_date) {
		try {
			SimpleDateFormat e = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			Date date = e.parse(str_date);
			Timestamp timeStampDate = new Timestamp(date.getTime());
			return timeStampDate;
		} catch (ParseException arg3) {
			System.out.println("Exception :" + arg3);
			return null;
		}
	}

	public static String parsePeriod(Period period) {
		String result = "";
		if (period != null) {
			int days = period.getDays();
			int hours = period.getHours();
			int minutes = period.getMinutes();
			int seconds = period.getSeconds();
			boolean d = false;
			boolean h = false;
			boolean m = false;
			int m1;
			if (seconds >= 60) {
				m1 = seconds / 60;
				int s = seconds % 60;
				seconds = s;
				minutes += m1;
			}

			if (minutes >= 60) {
				int h1 = minutes / 60;
				m1 = minutes % 60;
				hours += h1;
				minutes = m1;
			}
			
			if (hours >= 24) {
				int d1 = hours / 24;
				m1 = hours % 24;
				hours += m1;
				
				m1 = hours % 60;
				minutes = m1;
				days = d1;
			}
			
			if (minutes < 10) {
				result = String.format("%dD %dH:0%dM:%dS",
					new Object[]{Integer.valueOf(days), Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds)});
			} else if (minutes >= 10) {
				result = String.format("%dD %dH:%dM:%dS",
					new Object[]{Integer.valueOf(days), Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds)});
			}
		} else {
			result = String.format("0:00:00", new Object[0]);
		}

		return result;
	}

	public Collection<WorkOrder> filterWO(Collection<WorkOrder> workOrders) throws FileNotFoundException, IOException {
		Period zero = Period.ZERO;
		ArrayList result = new ArrayList();
		Iterator arg4 = workOrders.iterator();

		while (arg4.hasNext()) {
			WorkOrder workOrder = (WorkOrder) arg4.next();
			if (workOrder.getStatus().equalsIgnoreCase("closed")
					&& workOrder.getStatus().equalsIgnoreCase("Resolved")) {
				result.add(workOrder);
			}
		}

		return result;
	}

	public void writeWorkOrder(Sheet sheet, WorkOrder workOrder, Row row, boolean b) {
	}
}