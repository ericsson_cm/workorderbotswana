package com.ericsson.wo.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ericsson.utils.AllUtilities;

public class WorkOrder {
	private String NocRefId;
	private String customer;
	private String manuallyCreated;
	private String title;
	private int priority;
	private Period priorityTime;
	private Period currentTime;
	private Period totalTime;
	private Period calculated;
	private Period delta;
	private Period totalTtr;
	private double timeSLA;
	private double taskkey;
	public String status;
	private String site;
	private String FSO;
	private String cluster;
	private String woType;
	private DateTime creationDate;
	private DateTime assignmentStartDate;
	private DateTime assignmentFinishDate;
	private String assignedFT;
	private String serviceArea3;
	private String region;
	private String serviceArea4;
	private String office;
	private DateTime lastStatusDate;
	private String reasonforStatusChange;
	private String solutionReport;
	private Period timeInUnassigned;
	public Period timeSinceLastStatus;
	private Period timeInAssigned;
	private Period timeInDispatched;
	private Period timeInAccepted;
	private Period timeInRejectedFromField;
	private Period timeInInProgress;
	private Period timeInResolved;
	private Period timeInRejected;
	private Period timeInCancelled;
	private Period timeInWaitingInternal;
	private Period timeInWaitingExternal;
	private Period timeInRequestInProgress;
	private Period timeInRequestReOpen;
	private Period timeInObjectReady;
	private Period timeInTravel;
	private String statusBeforeChange;

	public String getFSO() {
		return this.FSO;
	}

	public void setFSO(String fSO) {
		this.FSO = fSO;
	}

	public Period getCurrentTime() {
		return this.currentTime;
	}

	public void setCurrentTime(Period currentTime) {
		this.currentTime = currentTime;
	}

	public Period sumPeriod() {
		Period result = Period.ZERO;
		result = result.plus(this.timeInUnassigned);
		result = result.plus(this.timeInAssigned);
		result = result.plus(this.timeInDispatched);
		result = result.plus(this.timeInAccepted);
		result = result.plus(this.timeInRejectedFromField);
		result = result.plus(this.timeInInProgress);
		result = result.plus(this.timeInResolved);
		result = result.plus(this.timeInRejected);
		result = result.plus(this.timeInCancelled);
		result = result.plus(this.timeInWaitingInternal);
		result = result.plus(this.timeInWaitingExternal);
		result = result.plus(this.timeInRequestInProgress);
		result = result.plus(this.timeInRequestReOpen);
		result = result.plus(this.timeInObjectReady);
		result = result.plus(this.timeInTravel);
		return result;
	}

	public Period sumPeriod2() {
		Period result = Period.ZERO;
		result = result.plus(this.timeInUnassigned);
		result = result.plus(this.timeInAssigned);
		result = result.plus(this.timeInDispatched);
		result = result.plus(this.timeInAccepted);
		result = result.plus(this.timeInRejectedFromField);
		result = result.plus(this.timeInInProgress);
		result = result.plus(this.timeInResolved);
		result = result.plus(this.timeInRejected);
		result = result.plus(this.timeInCancelled);
		result = result.plus(this.timeInWaitingInternal);
		result = result.plus(this.timeInRequestInProgress);
		result = result.plus(this.timeInRequestReOpen);
		result = result.plus(this.timeInObjectReady);
		result = result.plus(this.timeInTravel);
		return result;
	}

	public double getTaskkey() {
		return this.taskkey;
	}

	public void setTaskkey(double taskkey) {
		this.taskkey = taskkey;
	}

	public String getNocRefId() {
		return this.NocRefId;
	}

	public Period getTimeSinceLastStatus() {
		return this.timeSinceLastStatus;
	}

	public void setTimeSinceLastStatus(Period timeSinceLastStatus) {
		this.timeSinceLastStatus = timeSinceLastStatus;
	}

	public void setNocRefId(String NocRefId) {
		this.NocRefId = NocRefId;
	}

	public double getTimeSLA() {
		return this.timeSLA;
	}

	public void setTimeSLA(double timeSLA) {
		this.timeSLA = timeSLA;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
	}

	public String getOffice() {
		return this.office;
	}

	public void setOffice(String office) {
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getManuallyCreated() {
		return this.manuallyCreated;
	}

	public void setManuallyCreated(String manuallyCreated) {
		this.manuallyCreated = manuallyCreated;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSite() {
		return this.site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getWoType() {
		return this.woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public DateTime getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}

	public DateTime getAssignmentStartDate() {
		return this.assignmentStartDate;
	}

	public void setAssignmentStartDate(DateTime assignmentStartDate) {
		this.assignmentStartDate = assignmentStartDate;
	}

	public DateTime getAssignmentFinishDate() {
		return this.assignmentFinishDate;
	}

	public void setAssignmentFinishDate(DateTime assignmentFinishDate) {
		this.assignmentFinishDate = assignmentFinishDate;
	}

	public String getAssignedFT() {
		return this.assignedFT;
	}

	public void setAssignedFT(String assignedFT) {
		this.assignedFT = assignedFT;
	}

	public String getServiceArea3() {
		return this.serviceArea3;
	}

	public void setServiceArea3(String serviceArea3) {
		this.serviceArea3 = serviceArea3;
	}

	public String getServiceArea4() {
		return this.serviceArea4;
	}

	public void setServiceArea4(String serviceArea4) {
		this.serviceArea4 = serviceArea4;
	}

	public DateTime getLastStatusDate() {
		return this.lastStatusDate;
	}

	public void setLastStatusDate(DateTime lastStatusDate) {
		this.lastStatusDate = lastStatusDate;
	}

	public String getReasonforStatusChange() {
		return this.reasonforStatusChange;
	}

	public void setReasonforStatusChange(String reasonforStatusChange) {
		this.reasonforStatusChange = reasonforStatusChange;
	}

	public String getSolutionReport() {
		return this.solutionReport;
	}

	public void setSolutionReport(String solutionReport) {
		this.solutionReport = solutionReport;
	}

	public Period getTimeInUnassigned() {
		return this.timeInUnassigned;
	}

	public void setTimeInUnassigned(Period timeInUnassigned) {
		this.timeInUnassigned = timeInUnassigned;
	}

	public Period getTimeInAssigned() {
		return this.timeInAssigned;
	}

	public void setTimeInAssigned(Period timeInAssigned) {
		this.timeInAssigned = timeInAssigned;
	}

	public Period getTimeInDispatched() {
		return this.timeInDispatched;
	}

	public void setTimeInDispatched(Period timeInDispatched) {
		this.timeInDispatched = timeInDispatched;
	}

	public Period getTimeInAccepted() {
		return this.timeInAccepted;
	}

	public void setTimeInAccepted(Period timeInAccepted) {
		this.timeInAccepted = timeInAccepted;
	}

	public Period getTimeInRejectedFromField() {
		return this.timeInRejectedFromField;
	}

	public void setTimeInRejectedFromField(Period timeInRejectedFromField) {
		this.timeInRejectedFromField = timeInRejectedFromField;
	}

	public Period getTimeInInProgress() {
		return this.timeInInProgress;
	}

	public void setTimeInInProgress(Period timeInInProgress) {
		this.timeInInProgress = timeInInProgress;
	}

	public Period getTimeInResolved() {
		return this.timeInResolved;
	}

	public void setTimeInResolved(Period timeInResolved) {
		this.timeInResolved = timeInResolved;
	}

	public Period getTimeInRejected() {
		return this.timeInRejected;
	}

	public void setTimeInRejected(Period timeInRejected) {
		this.timeInRejected = timeInRejected;
	}

	public Period getTimeInCancelled() {
		return this.timeInCancelled;
	}

	public void setTimeInCancelled(Period timeInCancelled) {
		this.timeInCancelled = timeInCancelled;
	}

	public Period getTimeInWaitingInternal() {
		return this.timeInWaitingInternal;
	}

	public void setTimeInWaitingInternal(Period timeInWaitingInternal) {
		this.timeInWaitingInternal = timeInWaitingInternal;
	}

	public Period getTimeInWaitingExternal() {
		return this.timeInWaitingExternal;
	}

	public void setTimeInWaitingExternal(Period timeInWaitingExternal) {
		this.timeInWaitingExternal = timeInWaitingExternal;
	}

	public Period getTimeInRequestInProgress() {
		return this.timeInRequestInProgress;
	}

	public void setTimeInRequestInProgress(Period timeInRequestInProgress) {
		this.timeInRequestInProgress = timeInRequestInProgress;
	}

	public Period getTimeInRequestReOpen() {
		return this.timeInRequestReOpen;
	}

	public void setTimeInRequestReOpen(Period timeInRequestReOpen) {
		this.timeInRequestReOpen = timeInRequestReOpen;
	}

	public Period getTimeInObjectReady() {
		return this.timeInObjectReady;
	}

	public void setTimeInObjectReady(Period timeInObjectReady) {
		this.timeInObjectReady = timeInObjectReady;
	}

	public Period getTimeInTravel() {
		return this.timeInTravel;
	}

	public void setTimeInTravel(Period timeInTravel) {
		this.timeInTravel = timeInTravel;
	}

	public String getStatusBeforeChange() {
		return this.statusBeforeChange;
	}

	public void setStatusBeforeChange(String statusBeforeChange) {
		this.statusBeforeChange = statusBeforeChange;
	}

	public void calculPeriod() {
		DateTime now = new DateTime(DateTimeZone.UTC);
		LocalDate today = now.toLocalDate();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm");
		if (this.status.equalsIgnoreCase("Resolved")) {
			this.currentTime = AllUtilities.getAbsDifference(this.getCreationDate(), this.getLastStatusDate());
		} else {
			this.currentTime = AllUtilities.getAbsDifference(this.getCreationDate(), now);
		}

	}

	public void calculDouble() {
		this.timeSLA = AllUtilities.dividePeriod(this.currentTime, AllUtilities.getPriorityTime(this.getPriority()));
	}

	public void calculPeriod2() {
		DateTime now = new DateTime(DateTimeZone.UTC);
		LocalDate today = now.toLocalDate();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm");
		this.timeSinceLastStatus = AllUtilities.getAbsDifference(this.getLastStatusDate(), now);
	}

	public void calcul() {
		this.calculPeriod();
		this.calculDouble();
		this.calculPeriod2();
	}

	public String getCluster() {
		return this.cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
}
