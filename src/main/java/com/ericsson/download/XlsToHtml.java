package com.ericsson.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellRangeAddress;

public class XlsToHtml {
	private final StringBuilder out = new StringBuilder(65536);
	private final SimpleDateFormat sdf;
	private final HSSFWorkbook book;
	private final HSSFPalette palette;
	private final FormulaEvaluator evaluator;
	private short colIndex;
	private String excelFilePath;
	private int rowIndex;
	private int mergeStart;
	private int mergeEnd;
	private Map<Integer, Map<Short, List<HSSFPictureData>>> pix = new HashMap();
	FileInputStream excelFile;

	public XlsToHtml(FileInputStream excelFile) throws IOException {
		this.excelFile = new FileInputStream(new File(this.excelFilePath));
		this.sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (excelFile == null) {
			this.book = null;
			this.palette = null;
			this.evaluator = null;
		} else {
			this.book = new HSSFWorkbook(excelFile);
			HSSFSheet sheet = this.book.getSheet("GLOBAL");
			this.palette = this.book.getCustomPalette();
			this.evaluator = this.book.getCreationHelper().createFormulaEvaluator();

			for (int i = 0; i < this.book.getNumberOfSheets(); ++i) {
				this.table(this.book.getSheet("GLOBAL"));
			}

		}
	}

	public void table(HSSFSheet sheet) {
		if (sheet != null) {
			if (sheet.getDrawingPatriarch() != null) {
				List shapes = sheet.getDrawingPatriarch().getChildren();

				for (int i = 0; i < shapes.size(); ++i) {
					if (shapes.get(i) instanceof HSSFPicture) {
						try {
							HSSFShape e = (HSSFShape) shapes.get(i);
							Field f = HSSFShape.class.getDeclaredField("anchor");
							f.setAccessible(true);
							HSSFClientAnchor anchor = (HSSFClientAnchor) f.get(e);
							if (!this.pix.containsKey(Integer.valueOf(anchor.getRow1()))) {
								this.pix.put(Integer.valueOf(anchor.getRow1()), new HashMap());
							}

							if (!((Map) this.pix.get(Integer.valueOf(anchor.getRow1())))
									.containsKey(Short.valueOf(anchor.getCol1()))) {
								((Map) this.pix.get(Integer.valueOf(anchor.getRow1())))
										.put(Short.valueOf(anchor.getCol1()), new ArrayList());
							}

							((List) ((Map) this.pix.get(Integer.valueOf(anchor.getRow1())))
									.get(Short.valueOf(anchor.getCol1())))
											.add((HSSFPictureData) this.book.getAllPictures()
													.get(((HSSFPicture) e).getPictureIndex()));
						} catch (Exception arg6) {
							throw new RuntimeException(arg6);
						}
					}
				}
			}

			this.out.append("<table cellspacing=\'0\' style=\'border-spacing:0; border-collapse:collapse;\'>\n");

			for (this.rowIndex = 0; this.rowIndex < sheet.getPhysicalNumberOfRows(); ++this.rowIndex) {
				this.tr(sheet.getRow(this.rowIndex));
			}

			this.out.append("</table>\n");
		}
	}

	private void tr(HSSFRow row) {
		if (row != null) {
			this.out.append("<tr ");

			for (int i = 0; i < row.getSheet().getNumMergedRegions(); ++i) {
				CellRangeAddress merge = row.getSheet().getMergedRegion(i);
				if (this.rowIndex >= merge.getFirstRow() && this.rowIndex <= merge.getLastRow()) {
					this.mergeStart = merge.getFirstColumn();
					this.mergeEnd = merge.getLastColumn();
					break;
				}
			}

			this.out.append("style=\'");
			if (row.getHeight() != -1) {
				this.out.append("height: ").append(Math.round((double) row.getHeight() / 20.0D * 1.33333D))
						.append("px; ");
			}

			this.out.append("\'>\n");

			for (this.colIndex = 0; this.colIndex < row.getLastCellNum(); ++this.colIndex) {
				this.td(row.getCell(this.colIndex));
			}

			this.out.append("</tr>\n");
		}
	}

	private void td(HSSFCell cell) {
		int colspan = 1;
		if (this.colIndex == this.mergeStart) {
			colspan = this.mergeEnd - this.mergeStart + 1;
		} else {
			if (this.colIndex == this.mergeEnd) {
				this.mergeStart = -1;
				this.mergeEnd = -1;
				return;
			}

			if (this.mergeStart != -1 && this.mergeEnd != -1 && this.colIndex > this.mergeStart
					&& this.colIndex < this.mergeEnd) {
				return;
			}
		}

		this.out.append("<td ");
		if (colspan > 1) {
			this.out.append("colspan=\'").append(colspan).append("\' ");
		}

		if (cell == null) {
			this.out.append("/>\n");
		} else {
			this.out.append("style=\'");
			HSSFCellStyle style = cell.getCellStyle();
			switch (style.getAlignment()) {
				case 1 :
					this.out.append("text-align: left; ");
					break;
				case 2 :
					this.out.append("text-align: center; ");
					break;
				case 3 :
					this.out.append("text-align: right; ");
			}

			HSSFFont font = style.getFont(this.book);
			if (font.getBoldweight() == 700) {
				this.out.append("font-weight: bold; ");
			}

			if (font.getItalic()) {
				this.out.append("font-style: italic; ");
			}

			if (font.getUnderline() != 0) {
				this.out.append("text-decoration: underline; ");
			}

			this.out.append("font-size: ").append(Math.floor((double) font.getFontHeightInPoints() * 0.8D))
					.append("pt; ");
			short[] backRGB = style.getFillForegroundColorColor().getTriplet();
			HSSFColor foreColor = this.palette.getColor(font.getColor());
			if (foreColor != null) {
				short[] val = foreColor.getTriplet();
				if (val[0] != 0 || val[1] != 0 || val[2] != 0) {
					this.out.append("color: rgb(").append(val[0]).append(',').append(val[1]).append(',').append(val[2])
							.append(");");
				}
			}

			if (backRGB[0] != 0 || backRGB[1] != 0 || backRGB[2] != 0) {
				this.out.append("background-color: rgb(").append(backRGB[0]).append(',').append(backRGB[1]).append(',')
						.append(backRGB[2]).append(");");
			}

			if (style.getBorderTop() != 0) {
				this.out.append("border-top-style: solid; ");
			}

			if (style.getBorderRight() != 0) {
				this.out.append("border-right-style: solid; ");
			}

			if (style.getBorderBottom() != 0) {
				this.out.append("border-bottom-style: solid; ");
			}

			if (style.getBorderLeft() != 0) {
				this.out.append("border-left-style: solid; ");
			}

			this.out.append("\'>");
			String val1 = "";

			try {
				label87 : switch (cell.getCellType()) {
					case 0 :
						double pic = cell.getNumericCellValue();
						double e = (double) Math.round(pic);
						if (Math.abs(e - pic) < 1.0E-17D) {
							val1 = String.valueOf((int) e);
						} else {
							val1 = String.valueOf(pic);
						}
						break;
					case 1 :
						val1 = cell.getStringCellValue();
						break;
					case 2 :
						CellValue cv = this.evaluator.evaluate(cell);
						switch (cv.getCellType()) {
							case 0 :
								this.out.append(cv.getNumberValue());
								break label87;
							case 1 :
								this.out.append(cv.getStringValue());
							case 2 :
							case 3 :
							case 5 :
							default :
								break label87;
							case 4 :
								this.out.append(cv.getBooleanValue());
								break label87;
						}
					default :
						try {
							val1 = this.sdf.format(cell.getDateCellValue());
						} catch (Exception arg14) {
							;
						}
				}
			} catch (Exception arg15) {
				val1 = arg15.getMessage();
			}

			if ("null".equals(val1)) {
				val1 = "";
			}

			if (this.pix.containsKey(Integer.valueOf(this.rowIndex))
					&& ((Map) this.pix.get(Integer.valueOf(this.rowIndex))).containsKey(Short.valueOf(this.colIndex))) {
				for (Iterator arg8 = ((List) ((Map) this.pix.get(Integer.valueOf(this.rowIndex)))
						.get(Short.valueOf(this.colIndex))).iterator(); arg8.hasNext(); this.out.append("\'/>")) {
					HSSFPictureData pic1 = (HSSFPictureData) arg8.next();
					this.out.append("<img alt=\'Image in Excel sheet\' src=\'data:");
					this.out.append(pic1.getMimeType());
					this.out.append(";base64,");

					try {
						this.out.append(new String(Base64.encodeBase64(pic1.getData()), "US-ASCII"));
					} catch (UnsupportedEncodingException arg13) {
						throw new RuntimeException(arg13);
					}
				}
			}

			this.out.append(val1);
			this.out.append("</td>\n");
		}
	}

	public String getHTML() {
		return this.out.toString();
	}
}
