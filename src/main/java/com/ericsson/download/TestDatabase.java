package com.ericsson.download;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class TestDatabase {
	public static void main(String[] args) {
		PeriodFormatter formatter = (new PeriodFormatterBuilder()).appendHours().appendLiteral(":").appendMinutes()
				.toFormatter();
		Duration duration = formatter.parsePeriod("02:10").toStandardDuration();
		System.out.println(duration);
	}
}
