package com.ericsson.download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.spec.EncodedKeySpec;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlRadioButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Hdbdownload {
	private static String URL = "https://hdb.se.wfm.ericsson.net/hdb/pages/login/login.jsp";
	private HtmlElement form;

	public <Htmlradio> void downloadExcelFile(String username, String password, String filename) throws Throwable {
		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
		Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
		Logger.getLogger("org.apache.http").setLevel(Level.OFF);
		Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
		Throwable arg3 = null;
		Object arg4 = null;

		try {
			WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45);

			try {
				webClient.getOptions().setTimeout(0);
				webClient.getOptions().setCssEnabled(false);
				webClient.getOptions().setJavaScriptEnabled(true);
				webClient.getOptions().setThrowExceptionOnScriptError(false);
				webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
				HtmlPage page = (HtmlPage) webClient.getPage(URL);
				System.out.println(page);
				List window = page.getFrames();
				HtmlPage loginPage = null;
				System.out.println(window);
				System.out.println(window.size());
				if (window.size() > 2) {
					loginPage = (HtmlPage) ((FrameWindow) window.get(2)).getEnclosedPage();
					System.out.println(loginPage);
				}

				if (loginPage != null) {
					HtmlTextInput userInput = (HtmlTextInput) loginPage.getByXPath("//input[@name=\'userId\']").get(0);
					System.out.println(userInput);
					HtmlPasswordInput passInput = (HtmlPasswordInput) loginPage
							.getByXPath("//input[@name=\'password\']").get(0);
					System.out.println(passInput);
					HtmlSubmitInput submitButton = (HtmlSubmitInput) loginPage.getByXPath("//input[@name=\'submit\']")
							.get(0);
					System.out.println(submitButton);
					userInput.setValueAttribute(username);
					passInput.setValueAttribute(password);
					DateTime now = new DateTime(DateTimeZone.UTC);
					DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
					LocalDate today = now.toLocalDate();
					LocalDate yesterday = today.minusDays(1);
					LocalDate last7days = today.minusDays(7);
					LocalDate last28days = today.minusDays(28);
					LocalDate last35days = today.minusDays(35);
					HtmlPage reportPage = (HtmlPage) submitButton.click();
					HtmlSelect searchDateType = (HtmlSelect) reportPage.getByXPath("//select[@name=\'searchDateType\']")
							.get(0);
					searchDateType.setSelectedIndex(1);
					HtmlInput searchDateFrom = (HtmlInput) reportPage.getByXPath("//input[@name=\'searchDateFrom\']")
							.get(0);
					searchDateFrom.setAttribute("value",
							fmt.withZone(DateTimeZone.forID("Africa/Gaborone")).print(last7days));
					HtmlInput searchDateTo = (HtmlInput) reportPage.getByXPath("//input[@name=\'searchDateTo\']")
							.get(0);
					searchDateTo.setAttribute("value", fmt.withZone(DateTimeZone.forID("Africa/Gaborone")).print(today));
					//searchDateTo.setAttribute("value", fmt.withZone(DateTimeZone.forID("Africa/Bamako")).print(today));
					HtmlRadioButtonInput HistoricalOperationalDatabase = (HtmlRadioButtonInput) reportPage
							.getByXPath("//input[@value=\'HistoricalOperationalDatabase\']").get(0);
					HistoricalOperationalDatabase.click();
					HtmlCheckBoxInput megatask = (HtmlCheckBoxInput) reportPage.getElementByName("needMTSearch");
					megatask.setChecked(false);
					HtmlSubmitInput exportButton = (HtmlSubmitInput) reportPage
							.getByXPath("//input[@value=\'Export to Excel (With Reports)\']").get(0);
					int size = reportPage.getByXPath("//input[@name=\'exportToExcelButtonWithReport\']").size();
					
					WebResponse webResponse = exportButton.click().getWebResponse();
					
					//sSystem.out.println("Content >>" + webResponse.getContentAsString());

					try {
						InputStream input = webResponse.getContentAsStream();
						String contentType = webResponse.getContentType().toLowerCase(Locale.ENGLISH);
						File f1 = new File(filename);
						IOUtils.copy(input, new FileOutputStream(f1));
						
						
						
					} finally {
						webResponse.cleanUp();
					}
				}
			} finally {
				if (webClient != null) {
					webClient.close();
				}

			}

		} catch (Throwable arg44) {
			if (arg3 == null) {
				arg3 = arg44;
			} else if (arg3 != arg44) {
				arg3.addSuppressed(arg44);
			}

			throw arg3;
		}
	}
}
