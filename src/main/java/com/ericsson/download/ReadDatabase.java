package com.ericsson.download;

import org.joda.time.DateTime;
import org.joda.time.Period;

public class ReadDatabase {
	private double taskKey;
	private String customer;
	private String manuallyCreated;
	private String title;
	private int priority;
	private Period currentTime;
	private Period TimeSinceLastStatus;
	private double timeSLA;
	private String status;
	private String woType;
	private DateTime creationDate;
	private String assignedFT;
	private String region;
	private String office;
	private DateTime lastStatusDate;
	private String reasonforStatusChange;
	private String statusBeforeChange;

	public double getTaskKey() {
		return this.taskKey;
	}

	public void setTaskKey(double taskKey) {
		this.taskKey = taskKey;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getManuallyCreated() {
		return this.manuallyCreated;
	}

	public void setManuallyCreated(String manuallyCreated) {
		this.manuallyCreated = manuallyCreated;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Period getCurrentTime() {
		return this.currentTime;
	}

	public void setCurrentTime(Period currentTime) {
		this.currentTime = currentTime;
	}

	public Period getTimeSinceLastStatus() {
		return this.TimeSinceLastStatus;
	}

	public void setTimeSinceLastStatus(Period timeSinceLastStatus) {
		this.TimeSinceLastStatus = timeSinceLastStatus;
	}

	public double getTimeSLA() {
		return this.timeSLA;
	}

	public void setTimeSLA(double timeSLA) {
		this.timeSLA = timeSLA;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWoType() {
		return this.woType;
	}

	public void setWoType(String woType) {
		this.woType = woType;
	}

	public DateTime getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getAssignedFT() {
		return this.assignedFT;
	}

	public void setAssignedFT(String assignedFT) {
		this.assignedFT = assignedFT;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getOffice() {
		return this.office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public DateTime getLastStatusDate() {
		return this.lastStatusDate;
	}

	public void setLastStatusDate(DateTime lastStatusDate) {
		this.lastStatusDate = lastStatusDate;
	}

	public String getReasonforStatusChange() {
		return this.reasonforStatusChange;
	}

	public void setReasonforStatusChange(String reasonforStatusChange) {
		this.reasonforStatusChange = reasonforStatusChange;
	}

	public String getStatusBeforeChange() {
		return this.statusBeforeChange;
	}

	public void setStatusBeforeChange(String statusBeforeChange) {
		this.statusBeforeChange = statusBeforeChange;
	}
}
